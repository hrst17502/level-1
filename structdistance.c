#include<stdio.h>
#include<math.h>
struct co_ordinate
{
    float x;
    float y;
}; 
int main()
{
   float distance;
   struct co_ordinate p1, p2;
   printf("Enter the value of first co_ordinate\n");
   scanf("%f %f", &p1.x, &p1.y);
   printf("Enter the value of second co_ordinate\n");
   scanf("%f %f", &p2.x, &p2.y);
   distance=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
   printf("The distance between two co_ordinates (%f, %f) and (%f, %f) is %f", p1.x, p1.y, p2.x, p2.y, distance);
   return 0;
}


