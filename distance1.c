#include<stdio.h>
#include<math.h>

int main()
{
    float x1, y1, x2, y2;
    float distance;
    printf("enter the x and y values of first co-ordinates respectively\n");
    scanf("%f %f", &x1, &y1);
    printf("enter the x and y values of second co-ordinates respectively\n");
    scanf("%f %f", &x2, &y2);
    distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    printf("Distance between the two co-ordinates (%f, %f) and (%f, %f) is %f\n", x1, y1, x2, y2, distance);
    return 0;
}
