/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/


#include<stdio.h>
#include<math.h>
void input_points(float *x1, float *y1, float *x2, float *y2);
float compute_distance(float x1, float y1, float x2, float y2);
void output_result(float x1, float y1, float x2, float y2, float distance);
int main()
{
    float distance;
    float x1=0, x2=0, y1=0, y2=0;
    input_points(&x1, &y1, &x2, &y2);
    printf("The two co_ordinates are (%f, %f) and (%f, %f)\n", x1, y1, x2, y2);
    distance=compute_distance(x1, y1, x2, y2);
    output_result(x1, y1, x2, y2, distance);
    return 0;
}
void input_points(float *x1, float *y1, float *x2, float *y2)
{
   printf("Enter the value of the first co_ordinate\n");
   scanf("%f %f", x1, y1);
	printf("Enter the value of the second co_ordinate\n");
   scanf("%f %f", x2, y2);
}
float compute_distance(float x1, float y1, float x2, float y2)
{
    float distance;
    distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return distance;
}
void output_result(float x1, float y1, float x2, float y2, float distance)
{
    printf("The distance between two co_ordinates (%f, %f) and (%f, %f) is %f", x1, y1, x2, y2, distance);
}